#!/Users/xzen/.rvm/rubies/ruby-3.0.0/bin/ruby

class Microwave
  def initialize(s)
    @second = s.to_i
  end

  def timer
    r_s = @second % 60
    r_m = (@second - r_s) / 60
    puts "#{self.display(r_m)}:#{self.display(r_s)}"
  end

  def display(nb)
    if (nb.digits.length == 1)
      "0#{nb}"
    else
      "#{nb}"
    end
  end
end

Microwave.new(gets.chop).timer
