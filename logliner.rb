class LogLineParser
  def initialize(s)
    @message = s
  end

  def message
    v_start = @message.index(": ") + 2
    v_end = @message.index("\r")
    if (v_end == nil)
      v_end = @message.index("\n")
    elsif (v_end == nil)
      v_end = @message.length -1
    end
    @message[v_start...v_end]
  end

  def log_level
    v_start = @message.index("[") + 1
    v_end = @message.index("]")
    @message[v_start...v_end].downcase
  end

  def reformat
    msg = self.message
    log = self.log_level
    puts "#{msg} (#{log})"
  end
end

LogLineParser.new("[WARNING]: Disk almost full\r\n").reformat
