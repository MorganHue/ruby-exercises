require "date"

class Meetup
  def initialize(s)
    @description = s
  end

  def get_date
    myArray = @description.split(/ /)
    arrayDics = { "Monday" => 1, "Tuesday" => 2, "Wednesday" => 3, "Thursday" => 4, "Friday" => 5, "Saturday" => 6, "Sunday" => 7 }
    monthDic = { "January" => 1, "February" => 2, "March" => 3, "April" => 4, "May" => 5, "June" => 6, "July" => 7, "August" => 8, "September" => 9, "October" => 10, "November" => 1, "December" => 12 }
    year = myArray[myArray.length - 1].to_i
    month = monthDic[myArray[myArray.length - 2]]
    if (myArray.length == 5)
      day = myArray[myArray.length - 4]
      r = 18
    else
      thDics = { "first" => 1, "second" => 2, "third" => 3, "fourth" => 4, "fifth" => 5 }
      day = myArray[myArray.length - 4]
      r = 0
      th = myArray[1]
      if (th != "last")
        d = Date.new(year, month, 1)
        i = 0
        while i != thDics[th]
          while d.wday != arrayDics[day]
            d = d.next_day()
          end
          r = d.day
          d = d.next_day()
          i = i + 1
        end
      else
        d = Date.new(year, month, -1)
        while d.wday != arrayDics[day]
          d = d.prev_day
        end
        r = d.day
      end
    end
    puts "#{year}/#{month}/#{r}"
  end
end

Meetup.new(gets.chop).get_date
