class Lasagna
  EXPECTED_MINUTES_IN_OVEN = 40

  def remaining_minutes_in_oven(nb)
    Lasagna::EXPECTED_MINUTES_IN_OVEN - nb
  end

  def preparation_time_in_minutes(nb)
    nb * 2
  end

  def total_time_in_minutes(hash)
    puts preparation_time_in_minutes(hash[:number_of_layers]) + hash[:actual_minutes_in_oven]
  end
end

Lasagna.new.total_time_in_minutes(number_of_layers: 1, actual_minutes_in_oven: 30) #32
