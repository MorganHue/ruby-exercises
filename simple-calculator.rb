class SimpleCalculator
  def self.calculate(nb1, nb2, operator)
    if nb1.class != String && nb2.class != String && operator.class == String
      r = 0
      if operator == "+"
        r = nb1 + nb2
      elsif operator == "-"
        r = nb1 - nb2
      elsif operator == "*"
        r = nb1 * nb2
      elsif operator == "/"
        if nb1 != 0 && nb2 != 0
          r = nb1 / nb2
        else
          #puts("Division by zero is not allowed.")
          raise("Division by zero is not allowed")
        end
      else
        raise("UnsupportedOperation")
      end
      puts("#{nb1} #{operator} #{nb2} = #{r}")
    else
      raise("ArgumentError")
    end
  end
end
